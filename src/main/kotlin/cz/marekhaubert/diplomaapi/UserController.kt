package cz.marekhaubert.diplomaapi

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.DestinationVariable
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.*

/**
 * Created by Marek Haubert on 22.09.2017.
 */
@RestController
class UserController(private val userRepository: UserRepository, @Autowired private val template: SimpMessagingTemplate) {

    var log = Logger.getLogger(Application::class.java.getName())

    @GetMapping("/")
    fun findAll() = userRepository.findAll()

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long): User = userRepository.findOne(id)

    enum class Action(private val actionId: kotlin.Int) {
        CREATE(1), EDIT(2), DELETE(3);

        fun getActionId() = actionId
    }

    @MessageMapping("/user/{action}")
    @SendTo("/topic/users")
    fun handleUserAction(@DestinationVariable action: Int, user: User): User? {
        when (action) {
        // vytvořit uživatele
            Action.CREATE.getActionId() -> {
                val userResponse = userRepository.save(user)
                userResponse.actionId = action
                return userResponse
            }
        // editovat existujícího uživatele
            Action.EDIT.getActionId() -> {
                val userResponse = userRepository.save(user)
                userResponse.actionId = action
                return userResponse
            }
        // odstranit uživatele
            Action.DELETE.getActionId() -> {
                val userToDelete = userRepository.findOne(user.id)
                userRepository.delete(userToDelete.id)
                userToDelete.actionId = action
                return userToDelete
            }
        // chyba - neznámá operace
            else -> return null
        }
    }
}