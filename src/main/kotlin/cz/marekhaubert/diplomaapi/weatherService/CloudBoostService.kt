package cz.marekhaubert.diplomaapi.weatherService

import io.cloudboost.CloudApp
import io.cloudboost.CloudException
import io.cloudboost.CloudObject
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.servlet.ServletContext

/**
 * Created by Marek Haubert on 23.09.2017.
 */
@Service
class CloudBoostService @Autowired
constructor(context: ServletContext, private val weatherRestService: WeatherRestService) {

    private val logger = LogFactory.getLog(javaClass)
    private final val timer: Timer = Timer()

    init {
        CloudApp.init(APP_ID, APP_KEY)
        timer.schedule(object : TimerTask() {
            override fun run() {
                sendMessage()
            }
        }, 3600000, 3600000)
    }

    private fun sendMessage() {
        val temp = weatherRestService.weatherDto.main?.temp
        try {
            val weatherObject = CloudObject("Temperature")
            weatherObject.set("value", temp)
            weatherObject.save { x, t -> logger.debug(x.id) }
        } catch (e: CloudException) {
            e.printStackTrace()
        }

    }

    companion object {
        private val APP_ID = "guelfrkejkzs"
        private val APP_KEY = "6ff3346a-b651-449a-af51-91b990040b86"
    }
}