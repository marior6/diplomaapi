package cz.marekhaubert.diplomaapi.weatherService

/**
 * Created by Marek Haubert on 22.09.2017.
 */
class WeatherDto {

    var main: Main? = null

    inner class Main {
        var temp: Float = 0.toFloat()
        var pressure: Float = 0.toFloat()
        var humidity: Float = 0.toFloat()
        var temp_min: Float = 0.toFloat()
        var temp_max: Float = 0.toFloat()
    }
}