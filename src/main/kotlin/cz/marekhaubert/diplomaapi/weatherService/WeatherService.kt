package cz.marekhaubert.diplomaapi.weatherService

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

/**
 * Created by Marek Haubert on 22.09.2017.
 */
@Service
class WeatherRestService(restTemplateBuilder: RestTemplateBuilder) {

    private final val restTemplate: RestTemplate

    init {
        restTemplate = restTemplateBuilder.build()
    }

    val weatherDto: WeatherDto
        get() = this.restTemplate.getForObject(weatherURL, WeatherDto::class.java)

    companion object {
        var weatherURL = "http://api.openweathermap.org/data/2.5/weather?lat=50.083972&lon=14.440983&appid=d4e8bb6941f970e67e00f4e2756eb0f9&units=metric"
    }
}