package cz.marekhaubert.diplomaapi

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Marek Haubert on 22.09.2017.
 */
@Entity
class User(
        val name: String,
        val email: String,
        val phone: String,
        val imageId: Int,
        @Transient var actionId: Int,
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = -1) {

    override fun toString(): String = "User(id=$id, firstName='$name')"
}
