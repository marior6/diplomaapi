package cz.marekhaubert.diplomaapi

import org.springframework.data.repository.CrudRepository

/**
 * Created by Marek Haubert on 22.09.2017.
 */
interface UserRepository : CrudRepository<User, Long> {
}
