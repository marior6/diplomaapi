package cz.marekhaubert.diplomaapi

import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.*

/**
 * Created by Marek Haubert on 23.09.2017.
 */
@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
class WebSocketConfig : AbstractWebSocketMessageBrokerConfigurer() {

    override fun configureMessageBroker(config: MessageBrokerRegistry) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/topic");
        config.setUserDestinationPrefix("/user");
    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/diploma").withSockJS()
    }

    override fun configureWebSocketTransport(registration: WebSocketTransportRegistration) {
        registration.setMessageSizeLimit(8 * 1024);
    }
}