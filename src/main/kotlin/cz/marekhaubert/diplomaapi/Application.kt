package cz.marekhaubert.diplomaapi

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer
import javax.servlet.ServletContext


/**
 * Created by Marek Haubert on 21.07.2017.
 */
@SpringBootApplication
class Application : SpringBootServletInitializer() {

    @Autowired
    lateinit var context: ServletContext

    private val log = LoggerFactory.getLogger(Application::class.java)

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder = application.sources(Application::class.java)

    companion object {
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
        }
    }
}